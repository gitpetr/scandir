class BuildDigestHash
  attr_reader :digest_hash, :scan_dir, :dir_name

  def initialize(dir_name)
    @digest_hash = {}
    @dir_name = dir_name
  end

  def build
    scan_dir.entries.each_with_object(digest_hash) do |entry_name, hash|
      hash[entry_name] = digest(entry_name) if entry_valid?(entry_name)
    end
  end

  private

  def entry_valid?(entry_name)
    return if File.directory?("#{dir_name}/#{entry_name}")
    return if File.stat("#{dir_name}/#{entry_name}").mode.to_s(8)[3..5].to_i < 640
    true
  end

  def scan_dir
    @scan_dir ||= Dir.new(dir_name)
  end

  def digest(file_name)
    Digest::SHA256.base64digest(File.read("#{dir_name}/#{file_name}"))
  end
end


if __FILE__ == $0
  puts "#{$0}"
end
