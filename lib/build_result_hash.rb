class BuildResultHash
  attr_reader :doubles_hash, :result_hash, :digest_hash

  def initialize(doubles_hash, digest_hash)
    @doubles_hash = doubles_hash
    @digest_hash = digest_hash
    @result_hash = {}
  end

  def build
    doubles_hash.each do |key, value|
      file = digest_hash.select { |_, v| v == key }.keys.first
      result_hash[file] = value
    end
    result_hash
  end
end
