require 'digest'
require_relative 'build_digest_hash'
require_relative 'build_doubles_hash'
require_relative 'build_result_hash'

class FileDoublesReport
  attr_reader :dir_name, :digest_hash, :doubles_hash, :build_digest_hash, :result_hash

  def initialize(dir_name)
    @dir_name = dir_name
    @digest_hash = {}
    @doubles_hash = Hash.new(0)
    @result_hash = {}
    @build_digest_hash = BuildDigestHash.new(dir_name)
  end

  def print
    abort 'Вы указали несуществующую папку' unless File.exist?(dir_name)
    digest_hash = build_digest_hash.build
    doubles_hash = BuildDoublesHash.new(digest_hash).build
    result_hash = BuildResultHash.new(doubles_hash, digest_hash).build
    puts 'Показано количество дубликатов файлов, название дубликата первое в списке:'
    puts result_hash
  end
end
