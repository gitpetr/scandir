class BuildDoublesHash
  attr_reader :digest_hash, :doubles_hash
  def initialize(digest_hash)
    @digest_hash = digest_hash
    @doubles_hash = Hash.new(0)
  end

  def build
    digest_hash.values.each_with_object(doubles_hash) do |element, hash|
      hash[element] += 1
    end
  end
end
